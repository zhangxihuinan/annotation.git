package com.diy.annotation.controller;

import com.diy.annotation.entity.vo.GoodsVO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * description: 备注
 *
 * @author ZhangXihui
 * @create 2020-01-03 14:26
 **/
@RestController
@RequestMapping(value = "/annotation")
public class TestJsonController {

    @GetMapping(value = "/field")
    public GoodsVO annotationField() {
        return new GoodsVO(1001L, "《基督山伯爵》", 38.23456789D);
    }

    @GetMapping(value = "/method")
    public GoodsVO annotationMethod() {
        return new GoodsVO();
    }

    @GetMapping(value = "/parameter")
    public GoodsVO annotationParameter() {
        return new GoodsVO();
    }
}
