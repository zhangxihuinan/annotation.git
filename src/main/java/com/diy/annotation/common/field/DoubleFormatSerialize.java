package com.diy.annotation.common.field;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.ContextualSerializer;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Objects;

/**
 * description: 自定义json序列化解析器
 *
 * createContextual可以获得字段的类型以及注解。
 * 当字段为Double类型并且拥有@DoubleFormat注解时，取出注解中的值创建定制的DoubleFormatSerialize，这样在serialize方法中便可以得到这个值了。
 * createContextual方法只会在第一次序列化字段时调用（因为字段的上下文信息在运行期不会改变），所以不用担心影响性能。
 *
 * @author ZhangXihui
 * @create 2020-01-03 11:30
 **/
public class DoubleFormatSerialize extends JsonSerializer<Double> implements ContextualSerializer {

    /**
     * 小数点后保留位数
     */
    private int bitNum;

    @Override
    public void serialize(Double aDouble, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        StringBuilder pattern = new StringBuilder("#0.");
        for (int i = 0; i < this.bitNum; i++) {
            // 追加位数
            pattern.append("0");
        }
        // 返回值
        jsonGenerator.writeString(new DecimalFormat(pattern.toString()).format(aDouble));
    }

    @Override
    public JsonSerializer<?> createContextual(SerializerProvider serializerProvider, BeanProperty beanProperty) throws JsonMappingException {
        if (beanProperty != null) {
            // 非 Double 直接跳过
            if (Objects.equals(beanProperty.getType().getRawClass(), Double.class)) {
                // 获取注解信息
                DoubleFormat annotation = beanProperty.getAnnotation(DoubleFormat.class);
                if (annotation == null) {
                    annotation = beanProperty.getContextAnnotation(DoubleFormat.class);
                }
                if (annotation != null) {
                    // 获得注解上的值并赋值
                    this.bitNum = annotation.value();
                    return this;
                }
            }
            return serializerProvider.findValueSerializer(beanProperty.getType(), beanProperty);
        }
        return serializerProvider.findNullValueSerializer(null);
    }
}
