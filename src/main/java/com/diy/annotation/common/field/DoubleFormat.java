package com.diy.annotation.common.field;

import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * description:double格式化注解
 *
 * @author ZhangXihui
 * @create 2020/1/3 11:28
 **/
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@JacksonAnnotationsInside
@JsonSerialize(using = DoubleFormatSerialize.class)
public @interface DoubleFormat {
    int value() default 2;
}
