package com.diy.annotation.entity.vo;

import com.diy.annotation.common.field.DoubleFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * description: 商品类
 *
 * @author ZhangXihui
 * @create 2020-01-03 11:24
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GoodsVO {

    private Long id;
    private String goodsName;
    /**商品名*/
    @DoubleFormat(value = 3)
    private Double price;
}
